import React from "react";

import MaterialUI from "./components/MaterialUI";
import Slider from "./components/Slider";

function App() {
  return (
    <>
      <MaterialUI />
      <Slider />
    </>
  );
}

export default App;
